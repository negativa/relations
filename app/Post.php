<?php

namespace App;

use App\Contracts\Likeable;
use Illuminate\Database\Eloquent\Model;

class Post extends Model implements Likeable
{
    use Concerns\Likeable;
}
