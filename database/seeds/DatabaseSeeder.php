<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run(): void
    {
         $this->call(UserSeeder::class);
         $this->call(PostSeeder::class);
         $this->call(PostLikesSeeder::class);
         $this->call(ArticleSeeder::class);
         $this->call(ArticleLikesSeeder::class);
    }
}
