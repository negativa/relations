<?php

use App\Article;
use App\User;
use Illuminate\Database\Seeder;

class ArticleLikesSeeder extends Seeder
{
    public function run(): void
    {
        $articles = Article::all();

        User::all()
            ->each(function (User $user) use ($articles) {
                $numberOfPosts = random_int(0, $articles->count());

                $articles->random($numberOfPosts)
                    ->each(function (Article $article) use ($user){
                        $user->addLike($article);
                    });
            });
    }
}
