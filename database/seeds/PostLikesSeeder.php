<?php

use App\Post;
use App\User;
use Illuminate\Database\Seeder;

class PostLikesSeeder extends Seeder
{
    public function run(): void
    {
        $posts = Post::all();

        User::all()
            ->each(function (User $user) use ($posts) {
                $numberOfPosts = random_int(0, $posts->count());

                $posts->random($numberOfPosts)
                    ->each(function (Post $post) use ($user){
                        $user->addLike($post);
                    });
            });
    }
}
